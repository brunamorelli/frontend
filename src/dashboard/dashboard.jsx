import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { getSummary } from './dashboardActions'
import Content from '../common/template/content'
import ContentHeader from '../common/template/contentHeader'
import ValueBox from '../common/widget/valueBox'

class Dashboard extends Component {

    componentWillMount() {
        this.props.getSummary()
    }

    render() {
        const { credit, debt } = this.props.summary
        return (
            <div>
                <ContentHeader title='Dashboard' subTitle='Versão 1.0' />
                <Content>
                    <ValueBox cols='12 4' color='success' icon='fas fa-university'
                        value={`R$ ${credit}`} text='Total de Créditos' />

                    <ValueBox cols='12 4' color='danger' icon='fas fa-credit-card'
                        value={`R$ ${debt}`} text='Total de Débitos' />

                    <ValueBox cols='12 4' color='info' icon='far fa-money-bill-alt'
                        value={`R$ ${credit - debt}`} text='Valor Consolidado' />

                </Content>
            </div>
        )
    }


}

const mapStateToProps = state => ({ summary: state.dashboard.summary })
const mapDispatchToProps = dispatch => bindActionCreators({ getSummary }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)