// import './common/template/dependencies'
import './App.css'
import { HashRouter } from 'react-router-dom'
import Routes from './routes'
import Header from './common/template/header'
import Sidebar from './common/template/sideBar'
import Footer from './common/template/footer'
import Messages from './common/msg/messages'


export default props =>
   (
    <HashRouter>
      <Header />
      <Sidebar />
      <div className="content-wrapper">
        <Routes />
      </div>

      <Footer />
      <Messages />
      
      <aside className="control-sidebar control-sidebar-dark"></aside>
    </HashRouter>
  )

