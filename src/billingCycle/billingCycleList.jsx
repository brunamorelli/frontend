import React, { Component } from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { getList, showTab } from './billingCycleActions'

class BillingCycleList extends Component {

    componentWillMount() {
        this.props.getList()
    }

    renderRows(){
        const list = this.props.list || []

        return list.map(bc => (
            <tr key={bc._id}>
                <td>{bc.name}</td>
                <td>{bc.month}</td>
                <td>{bc.year}</td>
                <td>
                    <button className="btn btn-warning"
                            onClick={() => this.props.showTab(bc, 'tabUpdate')}>
                                <i className="fas fa-pencil-alt"></i>
                            </button>
                    <button className="btn btn-danger"
                            onClick={() => this.props.showTab(bc, 'tabDelete')}>
                                <i className="fas fa-trash-alt"></i>
                            </button>
                </td>
            </tr>
        ))
    }

    render() {
        return (
            <div>
                <table className="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Mês</th>
                            <th>Ano</th>
                            <th className='table-actions'>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderRows()}
                    </tbody>
                </table>
            </div>
        )
    }
}

const mapStateToProps = state => ({list: state.billingCycle.list})
const mapDispatchToProps = dispatch => bindActionCreators({getList, showTab}, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(BillingCycleList)