import React from 'react'
// import { Router, Route, Redirect } from 'react-router'
import AuthOrApp from './authOrApp'
import App from './App'
import { Switch, Route, Redirect } from 'react-router-dom'
import Dashboard from './dashboard/dashboard'
import BillingCycle from './billingCycle/billingCycle'

export default props => (
    <Switch>
        <Route path="/billingCycle">
            <BillingCycle />
        </Route>
        <Route path="/">
            <Dashboard />
        </Route>
        <Redirect path="*">
            <BillingCycle />
        </Redirect>
    </Switch>
)