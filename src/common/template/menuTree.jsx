import React from 'react';

export default props => (
    
        <li className="nav-item">
            <a href={props.path} className={`nav-link ${props.isLinkActive ? 'active' : ''}`}>
                <i className={`nav-icon ${props.icon}`}></i>
                <p>{props.label}</p>
            </a>
        </li>
        // <li className="nav-item">
        //     <a href="./index2.html" className="nav-link">
        //         <i className="far fa-circle nav-icon"></i>
        //         <p>Dashboard v2</p>
        //     </a>
        // </li>
   
)