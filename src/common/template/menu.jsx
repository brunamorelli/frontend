import React from 'react'
import MenuItem from './menuItem'
import MenuTree from './menuTree'

export default props => (
    <nav className="mt-2">
        <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            {/* <!-- Add icons to the links using the .nav-icon className
               with font-awesome or any other icon font library --> */}



            <MenuItem path="/#/" isLinkActive={false} icon="fas fa-tachometer-alt"
                label="Dashboard" treeView={false} />

            <MenuItem path="#" isLinkActive={false} icon="fas fa-edit"
                label="Cadastro" treeView={true} >
                <MenuTree path="#/billingCycle" isLinkActive={true} icon="fa fa-dollar-sign" label="Ciclo de Pagamento" />
            </MenuItem>

        </ul>
    </nav>


)