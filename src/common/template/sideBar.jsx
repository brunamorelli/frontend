import React from 'react'
import Menu from './menu'


export default props => (
    <aside className="main-sidebar sidebar-dark-primary elevation-4">
        {/* <!-- Brand Logo --> */}
        <a href="/#/" className="brand-link">
            {/* <img src="dist/img/AdminLTELogo.png" alt="My Money APP Logo" className="brand-image img-circle elevation-3"
                    style={{ opacity: ".8" }} /> */}
            <span className="brand-image  elevation-3" style={{ opacity: '.8' }}>
                <i className="fa fa-money-bill" style={{ paddingTop: '9px' }}></i>
            </span>
            <span className="brand-text"><strong> My</strong> Money App</span>

        </a>

        {/* <!-- Sidebar --> */}
        <div className="sidebar">

            {/* <!-- Sidebar Menu --> */}
            <nav className="mt-2">
                <Menu />
            </nav>
            {/* <!-- /.sidebar-menu --> */}
        </div>
        {/* <!-- /.sidebar --> */}
    </aside>
)