
// import 'admin-lte/plugins/jquery-ui/jquery-ui.min'
// import 'admin-lte/plugins/fastclick/fastclick'
// import 'admin-lte/plugins/overlayScrollbars/js/OverlayScrollbars.min'
// import 'admin-lte/dist/js/adminlte'
// import 'ionicons/dist/ionicons'

// import 'bootstrap/dist/css/bootstrap.min.css'
// import 'font-awesome/css/font-awesome.min.css'
// import 'admin-lte/dist/css/adminlte.min.css'
// import 'admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css'
// import 'admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css'

// import './custom.css'

// jQuery
import 'jquery/src/jquery'
//Moment
// import 'admin-lte/plugins/moment/moment.min.js'
// import 'admin-lte/plugins/moment/locales.js'
//Bootstrap 4
import 'admin-lte/plugins/bootstrap/js/bootstrap.bundle.min.js'
//ChartJS
import 'admin-lte/plugins/chart.js/Chart.min'
//Sparkline
import 'admin-lte/plugins/sparklines/sparkline'
//JQVMap
import 'admin-lte/plugins/jqvmap/jquery.vmap.min'
import 'admin-lte/plugins/jqvmap/maps/jquery.vmap.usa'
//jQuery Knob Chart
import 'admin-lte/plugins/jquery-knob/jquery.knob.min'
//daterangepicker
import 'admin-lte/plugins/daterangepicker/daterangepicker'
//Tempusdominus Bootstrap 4
// import 'admin-lte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min'
//Summernote
import 'admin-lte/plugins/summernote/summernote-bs4.min'
//overlayScrollbars
import 'admin-lte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min'
// AdminLTE App
import 'admin-lte/dist/js/adminlte'

//FontAwesome
import 'admin-lte/plugins/fontawesome-free/css/all.min.css'
//Ionicons
import 'ionicons/dist/ionicons/' // ver onde está esse css na documentação do componente
//Tempusdominus Bootstrap 4
import 'admin-lte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css'
//iCheck
import 'admin-lte/plugins/icheck-bootstrap/icheck-bootstrap.min.css'
//JQVMap
import 'admin-lte/plugins/jqvmap/jqvmap.min.css'
//Theme style
import 'admin-lte/dist/css/adminlte.min.css'
//overlayScrollbars
import 'admin-lte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css'
//Daterange picker
import 'admin-lte/plugins/daterangepicker/daterangepicker.css'
//summernote
import 'admin-lte/plugins/summernote/summernote-bs4.css'

import './custom.css'