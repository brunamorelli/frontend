import React from 'react'

export default props => (
    <footer className="main-footer">
        <strong>Copyright &copy; 2020 <a href="http://adminlte.io">AdminLTE.io</a>.</strong>
        All rights reserved.
        <div className="float-right d-none d-sm-inline-block">
        <b>Version</b> 3.0.5
        </div>
  </footer>
)