import React from 'react'

import Row from '../layout/row'

export default props => (

    <section className="content">
        <div className="container-fluid">

            <Row>
                {props.children}
            </Row>

        </div>
    </section>



)