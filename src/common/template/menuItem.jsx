import React from 'react'

export default props => {

    return (
        <li className={`nav-item ${props.treeView ? 'has-treeview' : ''}`}>
            <a href={props.path} className={`nav-link ${props.isLinkActive ? 'active' : ''}`}>
                <i className={`nav-icon ${props.icon}`}></i>
                <p>
                    {props.treeView ? <i className="right fas fa-angle-left"></i> : ''}
                    {props.label}
                </p>

            </a>

            {/* Quando possui treeview */}
            {props.treeView ?
                <ul className="nav nav-treeview">
                    {props.children}
                </ul>
                :
                ''
            }
        </li>
    )
}