import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { logout } from '../../auth/authActions'

class Navbar extends Component {
    constructor(props) {
        super(props)
        this.state = { open: false }
    }
    changeOpen() {
        this.setState({ open: !this.state.open })
    }
    render() {
        const { name, email } = this.props.user
        return (

            <ul className="navbar-nav ml-auto">

                <li className="nav-item dropdown user-menu">
                    <a href="#" className="nav-link dropdown-toggle" data-toggle="dropdown">
                        <img src="http://lorempixel.com/160/160/abstract" className="user-image img-circle elevation-2" alt="User Image" />
                        <span className="d-none d-md-inline">{name}</span>
                    </a>
                    <ul className="dropdown-menu dropdown-menu-lg dropdown-menu-right">

                        <li className="user-header bg-primary">
                            <img src="http://lorempixel.com/160/160/abstract" className="img-circle elevation-2" alt="User Image" />

                            <p>
                                {name}
                                <small>{email}</small>
                            </p>
                        </li>
                        <li className="user-footer">
                            <a href="#" onClick={this.props.logout} className="btn btn-default btn-flat float-right">Sair</a>
                        </li>
                    </ul>
                </li>
            </ul>
        )
    }
}
const mapStateToProps = state => ({ user: state.auth.user })
const mapDispatchToProps = dispatch => bindActionCreators({ logout }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Navbar)