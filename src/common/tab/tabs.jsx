import React from 'react'

import Grid from '../layout/grid'

export default props => (
    <Grid cols='12'>
        <div className='card card-primary card-outline card-outline-tabs'>
            {props.children}
        </div>
    </Grid>

)